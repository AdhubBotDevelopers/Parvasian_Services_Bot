const Commando = require('discord.js-commando');
var openDB = require('json-file-db');
var db = openDB('Votes/voters.json');
var times = openDB('Votes/VoterTimes.json');
const sqlite = require('sqlite');
const path = require('path');
const config = require('./config.json')
const client = new Commando.CommandoClient({
	owner: ['118455061222260736'],
	commandPrefix: '\\',
	disableEveryone: true
})
var fs = require('fs');
const token = yargs.token;

client.registry
	.registerGroups([
		['required', 'Required'],
		['misc', 'Miscellaneous']
	])
	.registerDefaults()
	.registerCommandsIn(path.join(__dirname, 'commands'));

client.setProvider(
	sqlite.open(path.join(__dirname, 'settings.sqlite3')).then(db => new Commando.SQLiteProvider(db))
).catch(console.error);

client.on('ready', () => {
	client.user.setActivity('with all our amazing partners!');
	console.log('Reporting for duty!');
});

client.on('guildMemberAdd', member => {
	var banList = openDB('DB/GlobalBanList.json');
	banList.get({user: member.id}, function(err, data) {
		if (data) {
			member.kick(`Globally banned from the Sovereignty of Parvus.`);
			console.log(member.user.username + ' tried to join! His id is ' + member.id)
		}
	})
})

client.on('guildCreate', guild => {

	fs.writeFile("Guild-Owners/" + guild.ownerId + ".txt", guild.id, function (err) {
		if (err) {
			return; //wwww(err);
		}

		//wwww("The file was saved!");
	});
});

client.login(token);


const authors = [];
var warned = [];
var banned = [];
var messagelog = [];

const warnBuffer = 3;
const maxBuffer = 5;
const interval = 1000;
const warningMessage = "Please cease your spamming!";
const banMessage = "did not cease and desist their activities";
const maxDuplicatesWarning = 7;
const maxDuplicatesBan = 10;

function dd(bot) {
	bot.on('message', msg => {

		if (msg.author.id != bot.user.id) {
			var now = Math.floor(Date.now());
			authors.push({
				"time": now,
				"author": msg.author.id
			});
			messagelog.push({
				"message": msg.content,
				"author": msg.author.id
			});

			// Check how many times the same message has been sent.
			var msgMatch = 0;
			for (var i = 0; i < messagelog.length; i++) {
				if (messagelog[i].message == msg.content && (messagelog[i].author == msg.author.id) && (msg.author.id !== bot.user.id)) {
					msgMatch++;
				}
			}
			// Check matched count
			if (msgMatch == maxDuplicatesWarning && !warned.includes(msg.author.id)) {
				warn(msg, msg.author.id);
			}
			if (msgMatch == maxDuplicatesBan && !banned.includes(msg.author.id)) {
				ban(msg, msg.author.id);
			}

			var matched = 0;

			for (var i = 0; i < authors.length; i++) {
				if (authors[i].time > now - interval) {
					matched++;
					if (matched == warnBuffer && !warned.includes(msg.author.id)) {
						warn(msg, msg.author.id);
					} else if (matched == maxBuffer) {
						if (!banned.includes(msg.author.id)) {
							ban(msg, msg.author.id);
						}
					}
				} else if (authors[i].time < now - interval) {
					authors.splice(i);
					warned.splice(warned.indexOf(authors[i]));
					banned.splice(warned.indexOf(authors[i]));
				}
				if (messagelog.length >= 200) {
					messagelog.shift();
				}
			}
		}
	});

	function warn(msg, userid) {
		if (userid != 235088799074484224) {
			warned.push(msg.author.id);
			msg.channel.send(msg.author + " " + warningMessage);
		}
	}


	function ban(msg, userid) {
		for (var i = 0; i < messagelog.length; i++) {
			if (messagelog[i].author == msg.author.id) {
				messagelog.splice(i);

			}
		}

		banned.push(msg.author.id);

		var role = msg.guild.roles.find(val => val.name == "AdHub-Mute");
		msg.guild.members.find(val => val.id == msg.author.id).addRole(role);
		setTimeout(function () {
			msg.guild.members.find(val => val.id == msg.author.id).removeRole(role);
			msg.reply("Mute is up, please do not spam");
		}, 300000);

		msg.reply(" did not cease and desist");
	}
}
