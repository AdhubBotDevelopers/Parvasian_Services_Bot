const { Command } = require('discord.js-commando');
const openDB = require('json-file-db');

module.exports = class GlobalBanCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'globalban',
      group: 'required',
      memberName: 'globalban',
      description: 'Globally bans a member from all Parvasian Protectorates',
      examples: ['globalban <MENTION> <REASON>', 'globalban @Dr. Everett Mann#2318 HE IS SNEK'],
      args: [
        {
          key: 'victim',
          prompt: 'Please mention the person you wish to ban',
          type: 'member'
        },
        {
          key: 'reason',
          prompt: 'Please enter a reason',
          type: 'string'
        }
      ]
    })
  }  
  run(message, { victim, reason }) {
    var modList = openDB('DB/ModList.json');
    modList.get({user: message.author.id}, function(err, data) {
      if(data.length != 0) {
        var banList = openDB('DB/GlobalBanList.json');
        banList.put({user: victim.user.id, reason: reason}, function(err) {
          if (err != null) {
            this.logger.error(err);
            message.say(`Encountered an error while banning!`);
          } else {
            message.say(`Successfully globally banned ${victim.user.username} because ${reason}`);
            this.logger.debug(`Successfully globally banned ${victim.user.username} because ${reason}`);
          }
        });
        if (victim.bannable) {
          victim.ban(reason);
        } else {
          message.say(`I couldn't ban ${victim.username}. Check that they're not higher than I am, and that I have permission to ban members.`)
        }
        //TODO: Ban them from all other guilds
        var guildColl = this.client.guilds;
        for (var i = 0; i<guildColl.length; i++) {
            guildColl[i].ban(victim.id, {reason: "Globally Banned from the AdHub network."});
        }
      } else {
        message.say(`You don't have permissions to use this command!`);
      }

  });
}
}
