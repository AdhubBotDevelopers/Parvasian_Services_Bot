const { Command } = require('discord.js-commando');
const openDB = require('json-file-db');

module.exports = class VerifyCommand extends Command {
  constructor(client) {
    super(client, {
      name: 'verify',
      group: 'required',
      memberName: 'verify',
      description: 'Allows Global Moderators to verify that they are, in fact, global moderators',
      examples: ['verify'],
      guildOnly: true
    });
  }
  run(message) {
    var modList = openDB('DB/ModList.json');
    modList.get({user: message.author.id}, function(err, data) {
      if (data.length != 0) {
        message.say(`Yes! ${message.author.username} is a global moderator and can be trusted!`);
      } else {
        message.say(`No, ${message.author.username} is ***not*** a global moderator, do not trust them!`);
      }
    });
  }
}
