const { Command } = require('discord.js-commando');
const openDB = require('json-file-db');
const callbanList = openDB('DB/CallbanList.json');

module.exports = class CallbanCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'callban',
            group: 'required',
            memberName: 'callban',
            description: 'Bans a player from calling for a moderator',
            examples: ['callban <USER> <REASON>', 'callban @Dr. Everett Mann#2318 THE SNEKS ARE NOT COMING'],
            args: [
                {
                    key: 'user',
                    prompt: 'Please mention the person you wish to callban',
                    type: 'member'
                },
                {
                    key: 'reason',
                    prompt: 'Please enter a reason',
                    type: 'string'
                }
            ]
        });
    }
    run(message, { user, reason }) {
        var modList = openDB('DB/ModList.json');
        modList.get({user: message.author.id}, function(err, data) {
            if (err) throw err;
            if (data != null) {
                callbanList.put({user: user.id, reason: reason});
                message.say(`Successfully callbanned ${user.username}`);
            } else {
                message.say('You do not have permission to use this command, and this infraction has been logged.');
            }
        })
    }
}
