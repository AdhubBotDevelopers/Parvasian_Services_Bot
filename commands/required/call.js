const { Command } = require('discord.js-commando');
const openDB = require('json-file-db')

module.exports = class CallModCommand extends Command {
    constructor(client) {
        super(client, {
            name: 'call',
            group: 'required',
            memberName: 'call',
            description: 'Calls a Global Moderator',
            examples: ['call <Reason>', 'call THE SNEKS ARE EVERYWHERE'],
            args: [
                {
                    key: 'reason',
                    prompt: 'Please enter a reason',
                    type: 'string',
                    default: 'I need help!'
                }
            ]
        });
    }

    run(message, { reason }) {
        var callbanList = openDB('DB/CallbanList.json');
        callbanList.get({user: message.author.id}, function(err, data) {
            if (err) throw err;
            if (data.length != 0) {
                message.say(`You have been callbanned!`);
                return;
            } else {
                if (message.guild.available) {
                    const guild = this.client.guilds.get('400734359226810378');
                    const channel = guild.channels.get('400769467606368256')
                    console.log('Fetching invites');
                    var inviteChannel = message.guild.systemChannel;
                    if (inviteChannel == null) {
                        message.say('Could not find the invite channel!');
                        return;
                    }
                    var invite = inviteChannel.createInvite({
                        maxUses: 1,
                        reason: 'Someone called for a moderator!'
                    }).then(invite => 
                
                    channel.send(`${message.author} needs help in ${message.guild.name} with invite https://discord.gg/`+ invite.code + ` and reason ${reason}`)
                    ).catch(err=>
                    {
                        message.say(`Could not find the guild! Please manually call for a global moderator in our Capital, http://discord.gg/zXWytCd`);
                    });
                } else {
                    message.say(`Could not find the guild! Please manually call for a global moderator in our Capital, http://discord.gg/zXWytCd`);
                }
            }
        })
        
    }
}
